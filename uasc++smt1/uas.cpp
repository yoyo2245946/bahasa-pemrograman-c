#include <iostream>
using namespace std;

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    // Meminta input dari pengguna
    cout << "Masukkan nilai Absen: ";
    cin >> absen;

    cout << "Masukkan nilai Tugas: ";
    cin >> tugas;

    cout << "Masukkan nilai Quiz: ";
    cin >> quiz;

    cout << "Masukkan nilai UTS: ";
    cin >> uts;

    cout << "Masukkan nilai UAS: ";
    cin >> uas;

    // Menampilkan input nilai
    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;

    // Meminta input dari pengguna untuk persentase nilai
    double persentaseAbsen, persentaseTugas, persentaseQuiz, persentaseUTS, persentaseUAS;

    cout << "Masukkan persentase Absen (0-100): ";
    cin >> persentaseAbsen;

    cout << "Masukkan persentase Tugas (0-100): ";
    cin >> persentaseTugas;

    cout << "Masukkan persentase Quiz (0-100): ";
    cin >> persentaseQuiz;

    cout << "Masukkan persentase UTS (0-100): ";
    cin >> persentaseUTS;

    cout << "Masukkan persentase UAS (0-100): ";
    cin >> persentaseUAS;

    // Menghitung nilai berdasarkan persentase yang diinputkan
    nilai = (persentaseAbsen * 0.01 * absen) + (persentaseTugas * 0.01 * tugas) + 
            (persentaseQuiz * 0.01 * quiz) + (persentaseUTS * 0.01 * uts) + 
            (persentaseUAS * 0.01 * uas);

    // Menentukan huruf mutu berdasarkan nilai
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    // Menampilkan hasil
    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}
