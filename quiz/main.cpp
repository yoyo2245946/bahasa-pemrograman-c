/* nama : Hilmi Nurpadilah
kelas : if - 1b 
nim : 301230004
*/

#include <stdio.h>

// fungsi untuk menghitung faktorial
unsigned long long faktorial(int n){
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

// fungsi untuk menghitung kombinasi C(n, r)
unsigned long long kombinasi(int n, int r){
    if (r > n) {
        return 0; // kombinasi tidak valid jika r > n
    } else {
        return faktorial(n) / (faktorial(r) * faktorial(n - r));
    }
}

int main(){
    int tingkat;

    // meminta input dari pengguna 
    printf("masukkan tingkat segitiga Pascal: ");
    scanf("%d", &tingkat);

    // menghasilkan segitiga Pascal dan menampilkannya
    for (int i = 0; i < tingkat; i++){
        // menampilkan ruang sebelum angka di setiap baris
        for (int j = 0; j < tingkat - i - 1; j++){
            printf(" ");
        }
        // menampilkan angka menggunakan rumus kombinasi 
        for (int j = 0; j <= i; j++){
            printf("% llu ", kombinasi(i, j));
        }
        printf("\n");
        }
        return 0;
}
